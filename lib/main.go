package main

import (
	"fmt"
	"os"
)

func main() {

	fmt.Printf("%s: %s\n", "DB_USER", getenv("DB_USER", "valor default"))
	fmt.Printf("%s: %s\n", "DB_PWD", getenv("DB_PWD", "valor default"))
	fmt.Printf("%s: %s\n", "DB_HOST", getenv("DB_HOST", "valor default"))
	fmt.Printf("%s: %s\n", "DB_POST", getenv("DB_POST", "valor default"))
	fmt.Printf("%s: %s\n", "DB_DATABASE", getenv("DB_DATABASE", "valor default"))
}

func getenv(key, defaultValue string) string {
	value, defined := os.LookupEnv(key)

	if !defined {
		return defaultValue
	}

	return value
}
